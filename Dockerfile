FROM openjdk:11-jre

COPY target/trade-api-6d-0.0.1-SNAPSHOT.jar app.jar

EXPOSE 8081

ENTRYPOINT ["java", "-jar", "/app.jar"]
