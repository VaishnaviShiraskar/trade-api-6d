use stockTrades;
drop table infoticker;
-- SNIPPET FOR STEP 1:
create table infoTicker (

symbol VARCHAR(100) NOT NULL,
name VARCHAR(1000) NOT NULL,
country VARCHAR(100) not null,
industry VARCHAR(100) not null,
PRIMARY KEY ( symbol )
);
use stockTrades;
-- SNIPPET FOR STEP 3:

alter table infoTicker
add column stockPrice float not null;

-- SNIPPET FOR STEP 5:
use stockTrades;

alter table infoTicker
add column watchList int not null;

-- SNIPPET FOR STEP 6:
use stockTrades;
delete from infoTicker where stockPrice = 0;

-- SNIPPETS TO CHECK DATA:
select * from infoticker;
select count(* ) from infoticker;
select * from infoTicker where watchList = 1;

-- IF ERROR WHILE DELETING/UPDATING ROWS:
