

#SCHEDULER:

import requests
import pymysql

def loadData():
    
    mydb = pymysql.connect(host='localhost',
    user='root',
    passwd='c0nygre',
    db='stocktrades')

    newTickerList = ["AAPL","GOOGL","HDB","INFY","AZRE","HDB","IBN","MMYT","RDY","SIFY",
                 "TTM","WIT","WNS","YTRA","CAJ","HMC","IX","MFG","MRM","MUFG",
                 "NMR","SMFG","SONY","TM","HOFVW","LNFA","MSFT","GOOG","AMZN","FB",
                 "TSLA","NVDA","V","JPM","JNJ","WMT","UNH","BAC","MA","PG",
                 "HD","DIS","PYPL","ADBE","CMCSA","NKE","PFE","CRM","ORCL","CSCO",
                 "LLY","NFLX","KO","XOM","DHR","VZ","ABT","INTC","TMO","PEP",
                 "ABBV","WFC","AVGO","COST","T","MRK","MS","CVX","MDT","MCD",
                 "TXN","TMUS","SE","UPS","CHTR","NEE","QCOM","HON","PM","INTU",
                 "MRNA","BMY","C","BLK","UNP","LOW","SCHW","GS","SBUX","AMD",
                 "AXP","BA","AMT","RTX","AMGN","NOW","IBM","AMAT","EL","ISRG",
                 "SQ","TGT","DE","CAT","GE","SNAP","JBHT","MMM","CVS","SPGI",
                 "SYK","ZM","LMT","PLD","ZTS","BKNG","ABNB","MO","ANTM","GILD",
                 "BX","TJX","ADP","SNOW","LRCX","MDLZ","USB"]

   


    #print(newTickerList)
    for item in newTickerList:
        tickerName = item
        print("Updated: ",tickerName)
        r = requests.get("https://jqjfct0r76.execute-api.us-east-1.amazonaws.com/default/PythonPandas?ticker="+tickerName+"&num_days=1")
        cursor = mydb.cursor()
        json_obj = r.json()
        #print(json_obj.keys())
        if(list(json_obj.keys())[0] =='ticker'):
            a=(json_obj["price_data"][0])
            cursor.execute("UPDATE infoTicker SET stockPrice =(%s) WHERE SYMBOL = %s",((a[1]),json_obj['ticker']))
        

    #close the connection to the database.
    mydb.commit()
    cursor.close()
    from datetime import datetime
    now = datetime.now()
    print("Data updated on:",now)




import schedule
import time

schedule.every().day.at("10:30").do(loadData)
while 1:
    schedule.run_pending()
    time.sleep(1)