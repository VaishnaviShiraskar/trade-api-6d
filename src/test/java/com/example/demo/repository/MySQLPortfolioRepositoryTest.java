package com.example.demo.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.entities.Portfolio;

@SpringBootTest
@AutoConfigureMockMvc
public class MySQLPortfolioRepositoryTest {
	@Autowired
	MySQLPortfolioRepository mySQLPortfolioRepository;

	@Test
	public void testGetPortfolio() {
		List<Portfolio> portfolio = mySQLPortfolioRepository.getPortfolio();

		assertThat(portfolio).isNotEmpty();
	}

	/*
	 * @Test public void testUpdatePortfolio() { String symbol = "AAPL"; //Only
	 * state the symbol that is present in the portfolio/order table
	 * 
	 * mySQLPortfolioRepository.updatePortfolio(symbol); }
	 */

	@Test
	public void testAddPortfolio() {
		String symbol = "AAPL";
		int volume = 20;
		float price = 148.6f;
		float amount = 2000;

		Portfolio portfolio = mySQLPortfolioRepository.addPortfolio(symbol, volume, price, amount);

		assertThat(portfolio).isNotNull();
	}

	@Test
	public void testGetPortfolioTicker() {
		List<String> tickers = mySQLPortfolioRepository.getPortfolioTicker();

		assertThat(tickers).isNotEmpty();
	}

}
