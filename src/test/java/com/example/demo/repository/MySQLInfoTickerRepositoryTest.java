package com.example.demo.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.entities.TickerInfo;

@SpringBootTest
@AutoConfigureMockMvc
public class MySQLInfoTickerRepositoryTest {
	@Autowired
	MySQLInfoTickerRepository mySQLInfoTickerRepository;
	
	@Test
	public void testGetWatchList() throws Exception{
		List<TickerInfo> watchList = mySQLInfoTickerRepository.getWatchList();
		
		assertThat(watchList).isNotEmpty();
		
		for(TickerInfo tickerInfo : watchList) {
			System.out.println("Ticker Symbol in watchList: " + tickerInfo.getSymbol());
		}
	}
	
	@Test
	public void testAddToWatchList() {
		String symbol = "TSLA";
		
		TickerInfo tickerInfo = mySQLInfoTickerRepository.addToWatchList(symbol);
		
		assertThat(tickerInfo).isNotNull();
		
		String symbol2 = "ugeagbera"; //Random symbol
		
		TickerInfo tickerInfo2 = mySQLInfoTickerRepository.addToWatchList(symbol2);
		
		assertThat(tickerInfo2).isNull();
	}
	
	@Test
	public void testDeleteFromWatchList() {
		String symbol = "TSLA";
		
		Map<String, String> outputMap = mySQLInfoTickerRepository.deleteFromWatchList(symbol);
		
		assertThat(outputMap).isNotNull();
	}
	
	@Test
	public void testGetTickerPrice() {
		String symbol = "GOOGL";
		Map<String, Float> outputMap = mySQLInfoTickerRepository.getTickerPrice(symbol);
		
		System.out.println(outputMap.get("price"));
		assertThat(outputMap).isNotNull();
		
	}
	
	@Test
	public void testGetAllTickers() {
		List<String> tickers = mySQLInfoTickerRepository.getAllTickers();
		
		assertThat(tickers).isNotEmpty();
	}
}
