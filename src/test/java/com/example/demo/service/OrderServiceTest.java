package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.example.demo.entities.Order;
import com.example.demo.repository.OrderRepository;

@ActiveProfiles("h2")
@SpringBootTest
@AutoConfigureMockMvc
public class OrderServiceTest {

	@Autowired
	MockMvc mockmvc;

	@InjectMocks
	OrderService orderService;

	@MockBean
	OrderRepository orderRepository;

	public List<Order> orders = new ArrayList<>();

	public List<String> tickers = new ArrayList<>();

	public Order order = new Order();
	public Order order2 = new Order();

	@SuppressWarnings("deprecation")
	@BeforeEach
	void setUp() throws Exception {

		order.setId(5);
		order.setBuyOrSell("buy");
		order.setPrice(146.89f);
		order.setTimestamp();
		order.setStockTicker("AMZN");
		order.setVolume(20);
		order.setStatusCode(1);

		order2.setId(6);
		order2.setBuyOrSell("buy");
		order2.setPrice(146.89f);
		order2.setTimestamp();
		order2.setStockTicker("TSLA");
		order2.setVolume(25);
		order2.setStatusCode(1);

		orders.add(order);
		orders.add(order2);
		tickers.add(order.getStockTicker());
		tickers.add(order.getStockTicker());
		MockitoAnnotations.initMocks(this);
		mockmvc = MockMvcBuilders.standaloneSetup(orderService).build();
	}

	@Test
	void testGetAllOrder() throws Exception {
		when(orderService.getAllOrders()).thenReturn(orders);
		assertEquals(orders, orderService.getAllOrders());

	}

	@Test
	void testGetOrderById() throws Exception {
		int id = 5;
		when(orderService.getOrder(id)).thenReturn(order);
		assertThat(order.getId()).isEqualTo(5);
	}

	@Test
	void testGetOrderByTicker() throws Exception {
		String stockTicker = "AMZN";
		when(orderService.getOrdersByTicker(stockTicker)).thenReturn(orders);
		assertEquals(orders, orderService.getOrdersByTicker(stockTicker));

	}

	private void assertEquals(List<Order> orders2, List<Order> allOrders) {
	}

}
