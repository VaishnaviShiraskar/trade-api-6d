package com.example.demo.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.example.demo.entities.Order;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@ActiveProfiles("h2")

@SpringBootTest
@AutoConfigureMockMvc
public class OrderControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testShouldReturnListSize3() throws Exception {

		MvcResult mvcResult = this.mockMvc.perform(get("/api/orders/")).andDo(print()).andExpect(status().isOk())
				.andReturn();

		List<Order> order = new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(),
				new TypeReference<List<Order>>() {
				});

		assertThat(order.size()).isGreaterThan(0);
	}

	/*
	 * @Test public void testCreateOrder() throws Exception { // 1. setup stuff
	 * String testTicker = "AAPL"; Float testPrice = 148.0f; String testBuyOrSell =
	 * "buy"; int testVol = 10; int testStatusCode = 1;
	 * 
	 * Order testOrder = new Order(); testOrder.setStockTicker(testTicker);
	 * testOrder.setPrice(testPrice); testOrder.setBuyOrSell(testBuyOrSell);
	 * testOrder.setVolume(testVol); testOrder.setStatusCode(testStatusCode);
	 * 
	 * ObjectMapper mapper = new ObjectMapper(); ObjectWriter ow =
	 * mapper.writer().withDefaultPrettyPrinter(); String requestJson =
	 * ow.writeValueAsString(testOrder);
	 * 
	 * System.out.println(requestJson);
	 * 
	 * // 2. call method under test MvcResult mvcResult = this.mockMvc
	 * .perform(post("/api/orders/").header("Content-Type",
	 * "application/json").content(requestJson))
	 * .andDo(print()).andExpect(status().isOk()).andReturn();
	 * 
	 * // 3. verify the results Order order = new
	 * ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(), new
	 * TypeReference<Order>() { });
	 * 
	 * assertThat(order.getStockTicker()).isEqualTo(testTicker);
	 * assertThat(order.getPrice()).isEqualTo(testPrice); }
	 */

}
