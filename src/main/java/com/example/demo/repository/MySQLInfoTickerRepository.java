package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.TickerInfo;

@Repository
public class MySQLInfoTickerRepository implements InfoRepository {

	@Autowired
	JdbcTemplate template;

	@Override
	public List<String> getAllTickers() {
		String sql = "SELECT symbol FROM infoTicker";
		return template.query(sql, new TickerRowMapper());

	}

	@Override
	public List<TickerInfo> getWatchList() {
		String sql = "SELECT symbol, name, country, industry, stockPrice, watchlist FROM infoTicker WHERE watchlist=1";
		return template.query(sql, new TickerObjectRowMapper());
	}

	@Override
	public TickerInfo addToWatchList(String symbol) {
		try {
			String sql = "UPDATE infoTicker SET watchlist=1 WHERE symbol=?";
			template.update(sql, symbol);

			String sql2 = "SELECT symbol, name, country, industry, stockPrice, watchlist FROM infoTicker WHERE symbol=?";
			TickerInfo watchListTicker = template.queryForObject(sql2, new TickerObjectRowMapper(), symbol);
			return watchListTicker;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public Map<String, String> deleteFromWatchList(String symbol) {
		// update watchlist to 0
		String sql = "UPDATE infoTicker SET watchlist=0 WHERE symbol=?";
		template.update(sql, symbol);

		Map<String, String> jsonMap = new HashMap<>();
		jsonMap.put("Success", "true");
		jsonMap.put("Symbol", symbol);
		return jsonMap;
	}

	@Override
	public Map<String, Float> getTickerPrice(String symbol) {
		String sql = "SELECT stockPrice from infoTicker WHERE symbol=?";
		float price = template.queryForObject(sql, new TickerPriceRowMapper(), symbol);

		Map<String, Float> jsonMap = new HashMap<>();
		jsonMap.put("price", price);
		return jsonMap;

	}
}

class TickerRowMapper implements RowMapper<String> {

	@Override
	public String mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		return new String(rs.getString("symbol"));
	}
}

class TickerObjectRowMapper implements RowMapper<TickerInfo> {
	@Override
	public TickerInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new TickerInfo(rs.getString("symbol"), rs.getString("name"), rs.getString("country"),
				rs.getString("industry"), rs.getFloat("stockPrice"), rs.getInt("watchlist"));
	}
}

class TickerPriceRowMapper implements RowMapper<Float> {
	@SuppressWarnings("deprecation")
	public Float mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Float(rs.getFloat("stockPrice"));
	}
}