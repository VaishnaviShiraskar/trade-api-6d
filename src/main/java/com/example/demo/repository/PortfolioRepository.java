package com.example.demo.repository;

import java.util.List;

import com.example.demo.entities.Portfolio;

public interface PortfolioRepository {
	public List<Portfolio> getPortfolio();

	Portfolio addPortfolio(String symbol, int volume, float price, float amount);

	List<String> getPortfolioTicker();

	public void updatePortfolio(String symbol, float sPrice, int vol, float amt);

	public void deletePortfolio(String symbol);

}
