package com.example.demo.repository;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.example.demo.entities.TickerInfo;

@Component
public interface InfoRepository {

	public List<String> getAllTickers();

	public List<TickerInfo> getWatchList();

	public TickerInfo addToWatchList(String symbol);

	public Map<String, String> deleteFromWatchList(String symbol);

	public Map<String, Float> getTickerPrice(String symbol);
}
