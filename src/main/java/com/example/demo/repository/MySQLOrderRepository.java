package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Order;

@Repository
public class MySQLOrderRepository implements OrderRepository {

	@Autowired
	JdbcTemplate template;

	@Override
	public List<Order> getAllOrders() {
		String sql = "SELECT id,stockTicker,price,volume,buyOrSell,statusCode,timestamp FROM orders";
		return template.query(sql, new OrderRowMapper());

	}

	@Override
	public Order getOrderById(int id) {

		String sql = "SELECT id,stockTicker,price,volume,buyOrSell,statusCode,timestamp FROM orders WHERE id=?";
		return template.queryForObject(sql, new OrderRowMapper(), id);
	}

	@Override
	public Order editOrder(Order order) {
		// TODO Auto-generated method stub
		String sql = "UPDATE orders SET price = ?, volume = ?, buyOrSell = ?, statusCode = ?, stockTicker = ? ,timestamp=?"
				+ "WHERE id = ?";

		template.update(sql, order.getPrice(), order.getVolume(), order.getBuyOrSell(), order.getStatusCode(),
				order.getStockTicker(), order.getTimestamp(), order.getId());

		return order;
	}

	@Override
	public int deleteOrder(int id) {
		String sql = "DELETE FROM orders WHERE id = ?";
		template.update(sql, id);
		return id;
	}

	@Override
	public Order addOrder(Order order) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO orders (price, volume, buyOrSell, statusCode, stockTicker,timestamp) "
				+ "VALUES(?,?,?,?,?,?)";

		template.update(sql, order.getPrice(), order.getVolume(), order.getBuyOrSell(), order.getStatusCode(),
				order.getStockTicker(), order.getTimestamp());

		return order;
	}

	@Override
	public List<Order> getOrdersByTicker(String ticker) {
		String sql = "SELECT id,stockTicker,price,volume,buyOrSell,statusCode,timestamp FROM orders WHERE stockTicker=?";
		return template.query(sql, new OrderRowMapper(), ticker);
	}

	@Override
	public List<String> getOrderTicker() {
		String sql = "SELECT distinct(stockTicker) FROM orders";
		return template.query(sql, new OrderTickerRowMapper());
	}

}

class OrderRowMapper implements RowMapper<Order> {

	@Override
	public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Order(rs.getInt("id"), rs.getString("StockTicker"), rs.getDouble("Price"), rs.getInt("Volume"),
				rs.getString("BuyOrSell"), rs.getInt("StatusCode"), rs.getString("Timestamp"));
	}
}

class OrderTickerRowMapper implements RowMapper<String> {
	@Override
	public String mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new String(rs.getString("stockTicker"));
	}
}
