package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Portfolio;

@Repository
public class MySQLPortfolioRepository implements PortfolioRepository {
	@Autowired
	JdbcTemplate template;

	@Override
	public List<Portfolio> getPortfolio() {
		// TODO Auto-generated method stub
		String sql = "SELECT stockTicker,volume,price,amount_invested FROM portfolio";
		return template.query(sql, new OrderRowMapper());

	}

	@Override
	public void updatePortfolio(String symbol, float sPrice, int vol, float amt) {

		String sql = "UPDATE portfolio SET price =?,volume = ?, amount_invested = ? WHERE stockTicker = ?";

		template.update(sql, sPrice, vol, amt, symbol);

	}

	@Override
	public Portfolio addPortfolio(String symbol, int volume, float price, float amount) {
		Portfolio portfolio = new Portfolio(symbol, volume, price, amount);
		String sql = "INSERT INTO portfolio " + "VALUES(?,?,?,?)";
		template.update(sql, price, volume, symbol, amount);
		return portfolio;
	}

	@Override
	public List<String> getPortfolioTicker() {
		String sql = "SELECT distinct(stockTicker) FROM portfolio";
		return template.query(sql, new StockTickerRowMapper());
	}

	class OrderRowMapper implements RowMapper<Portfolio> {

		@Override
		public Portfolio mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Portfolio(rs.getString("StockTicker"), rs.getInt("Volume"),

					rs.getFloat("Price"), rs.getFloat("amount_invested"));
		}
	}

	class StockTickerRowMapper implements RowMapper<String> {

		@Override
		public String mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new String(rs.getString("stockTicker"));

		}
	}

	@Override
	public void deletePortfolio(String symbol) {
		String sql = "DELETE FROM Portfolio WHERE stockTicker = ?";
		template.update(sql, symbol);

	}

}
