package com.example.demo.entities;

public class TickerInfo {
	private String symbol;
	private String name;
	private String country;
	private String industry;
	private float stockPrice;
	private int watchList;	//1 if it is in
	
	public TickerInfo(String symbol, String name, String country, String industry, float stockPrice, int watchList) {
		this.symbol = symbol;
		this.name = name;
		this.country = country;
		this.industry = industry;
		this.stockPrice = stockPrice;
		this.watchList = watchList;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getIndustry() {
		return industry;
	}
	
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	
	public float getStockPrice() {
		return stockPrice;
	}
	
	public void setStockPrice(float stockPrice) {
		this.stockPrice = stockPrice;
	}
	
	public int getWatchList() {
		return watchList;
	}
	
	public void setWatchList(int wishist) {
		this.watchList = wishist;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	
}
