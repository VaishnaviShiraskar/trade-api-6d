package com.example.demo.entities;

public class Portfolio {
	private String StockTicker;
	private int volume;
	private float price;
	private float amount;
	public Portfolio() {

	}

public Portfolio(String stockTicker, int volume, float price, float amount) {
		super();
		setStockTicker(stockTicker);
		this.setVolume(volume);
		this.setPrice(price);
		this.setAmount(amount);
}

public String getStockTicker() {
	return StockTicker;
}

public void setStockTicker(String stockTicker) {
	StockTicker = stockTicker;
}

public int getVolume() {
	return volume;
}

public void setVolume(int volume) {
	this.volume = volume;
}

public float getPrice() {
	return price;
}

public void setPrice(float price) {
	this.price = price;
}

public float getAmount() {
	return amount;
}

public void setAmount(float amount) {
	this.amount = amount;
}


}
