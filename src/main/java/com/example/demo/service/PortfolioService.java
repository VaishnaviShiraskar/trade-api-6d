package com.example.demo.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Portfolio;
import com.example.demo.repository.PortfolioRepository;

@Service
public class PortfolioService {
	@Autowired
	private PortfolioRepository repository;

	@Autowired
	private OrderService orderService;

	@Autowired
	private TickerInfoService tickerService;

	public List<Portfolio> getPortfolio() {
		List<String> tickers = orderService.getOrderTicker();
		for (String ticker : tickers) {
			updatePortfolio(ticker);
		}
		return repository.getPortfolio();
	}

	public List<String> getPortfolioTicker() {
		return repository.getPortfolioTicker();
	}

	public Portfolio addPortfolio(Portfolio portfolio) {
		// TODO Auto-generated method stub
		return repository.addPortfolio(portfolio.getStockTicker(), portfolio.getVolume(), portfolio.getPrice(),
				portfolio.getAmount());
	}

	public void updatePortfolio(String symbol) {
		HashMap<String, Integer> mapVol = orderService.getTotalVolume(symbol);
		Integer vol = mapVol.get(symbol);

		HashMap<String, Float> mapAmt = orderService.getAmountInvested(symbol);
		Float amt = mapAmt.get(symbol);

		Float sPrice = tickerService.getTickerPrice(symbol).get("price");
		if (vol == 0) {
			repository.deletePortfolio(symbol);
		} else {
			repository.updatePortfolio(symbol, sPrice, vol, amt);
		}
		;

	}
}
