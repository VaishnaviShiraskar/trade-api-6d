package com.example.demo.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.TickerInfo;
import com.example.demo.repository.InfoRepository;

@Service
public class TickerInfoService {
	@Autowired
	private InfoRepository repository;

	public List<TickerInfo> getWatchList() {
		return repository.getWatchList();
	}

	public TickerInfo addToWatchList(String symbol) {
		return repository.addToWatchList(symbol);
	}

	public Map<String, String> deleteFromWatchList(String symbol) {
		return repository.deleteFromWatchList(symbol);
	}

	public Map<String, Float> getTickerPrice(String symbol) {
		return repository.getTickerPrice(symbol);
	}
}
