package com.example.demo.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Order;
import com.example.demo.entities.Portfolio;
import com.example.demo.repository.InfoRepository;
import com.example.demo.repository.OrderRepository;

@Service
public class OrderService {

	@Autowired
	private OrderRepository repository;

	@Autowired
	private InfoRepository repository2;

	@Autowired
	private PortfolioService portfolioService;

	public List<Order> getAllOrders() {
		return repository.getAllOrders();
	}

	public Order getOrder(int id) {
		return repository.getOrderById(id);
	}

	public Order saveOrder(Order order) {
		order.setTimestamp();
		order.setPrice(repository2.getTickerPrice(order.getStockTicker()).get("price"));

		Order obj = repository.editOrder(order);
		portfolioService.updatePortfolio(obj.getStockTicker());

		return obj;
	}

	public Order newOrder(Order order) {

		order.setTimestamp();
		order.setPrice(repository2.getTickerPrice(order.getStockTicker()).get("price"));
		Order obj = repository.addOrder(order);

		List<String> tickers = portfolioService.getPortfolioTicker();
		if (tickers.contains(obj.getStockTicker())) {
			portfolioService.updatePortfolio(obj.getStockTicker());

		} else {

			Portfolio p = new Portfolio(obj.getStockTicker(), obj.getVolume(), (float) obj.getPrice(),
					(float) (obj.getPrice() * obj.getVolume()));
			portfolioService.addPortfolio(p);

		}

		return obj;
	}

	public int deleteOrder(int id) {
		Order obj = repository.getOrderById(id);
		int del = repository.deleteOrder(id);
		portfolioService.updatePortfolio(obj.getStockTicker());
		return del;
	}

	public List<Order> getOrdersByTicker(String ticker) {
		return repository.getOrdersByTicker(ticker);
	}

	public List<String> getAllTickers() {
		return repository2.getAllTickers();

	}

	public List<String> getOrderTicker() {
		return repository.getOrderTicker();

	}

	public HashMap<String, Integer> getTotalVolume(String symbol) {
		HashMap<String, Integer> map = new HashMap<String, Integer>();

		List<Order> orders = repository.getOrdersByTicker(symbol);
		Integer totalVolume = 0;
		for (Order order : orders) {
			String orderType = order.getBuyOrSell();
			if (orderType.equals("buy") && order.getStatusCode() == 2) {
				totalVolume = totalVolume + order.getVolume();
			} else if (orderType.equals("sell") && order.getStatusCode() == 2) {
				totalVolume = totalVolume - order.getVolume();

			}
		}
		map.put(symbol, totalVolume);
		return map;
	}

	public HashMap<String, Float> getAmountInvested(String symbol) {

		HashMap<String, Float> map = new HashMap<String, Float>();
		HashMap<String, Integer> newmap = getTotalVolume(symbol);
		Integer totalVolume = newmap.get(symbol);
		float stockPrice = repository2.getTickerPrice(symbol).get("price");
		float amountInvested = totalVolume * stockPrice;
		map.put(symbol, amountInvested);
		return map;

	}

	public List<HashMap<String, String>> getVolumeDetails() {
		List<String> tickers = getOrderTicker();

		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		for (String ticker : tickers) {
			// System.out.println(ticker);
			HashMap<String, String> map = new HashMap<String, String>();
			HashMap<String, Integer> newmap = getTotalVolume(ticker);
			Integer vol = newmap.get(ticker);
			map.put("Symbol", ticker);

			map.put("Total Volume", Integer.toString(vol));
			list.add(map);

		}

		return list;

	}

	public List<HashMap<String, String>> getAllAmountDetails() {
		List<String> tickers = getOrderTicker();

		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		for (String ticker : tickers) {
			// System.out.println(ticker);
			HashMap<String, String> map = new HashMap<String, String>();
			HashMap<String, Float> newmap = getAmountInvested(ticker);
			Float amt = newmap.get(ticker);
			map.put("Symbol", ticker);

			map.put("Total Amount", Float.toString(amt));
			list.add(map);

		}

		return list;

	}
}
