package com.example.demo.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.TickerInfo;
import com.example.demo.service.TickerInfoService;

@RestController
@RequestMapping("api/watch")
public class TickerInfoController {

	@Autowired
	TickerInfoService service;

	// https://locahost:8080/api/watch/
	@GetMapping(value = "/")
	@CrossOrigin
	public List<TickerInfo> getWatchList() {
		return service.getWatchList();
	}

	// https://locahost:8080/api/watch/add?symbol=AAPL
	@PutMapping(value = "/add")
	@CrossOrigin
	public TickerInfo getOrdersByTicker(@RequestParam(defaultValue = "", required = true) String symbol) {

		return service.addToWatchList(symbol);
	}

	// https://locahost:8080/api/watch/del?symbol=AAPL
	@PutMapping(value = "/del")
	@CrossOrigin
	public Map<String, String> deleteFromWatchList(@RequestParam(defaultValue = "", required = true) String symbol) {
		return service.deleteFromWatchList(symbol);
	}

	@GetMapping(value = "/price")
	@CrossOrigin
	public Map<String, Float> getTickerPrice(@RequestParam(defaultValue = "", required = true) String symbol) {
		return service.getTickerPrice(symbol);
	}

}
