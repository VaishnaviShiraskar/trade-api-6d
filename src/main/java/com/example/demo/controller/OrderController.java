package com.example.demo.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Order;
import com.example.demo.service.OrderService;

@RestController
@RequestMapping("api/orders")
public class OrderController {

	@Autowired
	OrderService service;

	@GetMapping(value = "/")
	@CrossOrigin
	public List<Order> getAllOrders() {
		return service.getAllOrders();
	}

	@GetMapping(value = "/{id}")
	@CrossOrigin
	public Order getOrderById(@PathVariable("id") int id) {
		return service.getOrder(id);
	}

	@GetMapping(value = "/tickerList")
	@CrossOrigin
	public List<String> getAllTickers() {
		return service.getAllTickers();
	}

	@GetMapping(value = "/orderTickerList")
	@CrossOrigin
	public List<String> getOrderTicker() {
		return service.getOrderTicker();
	}

	@PostMapping(value = "/")
	@CrossOrigin
	public Order addOrder(@RequestBody Order order) {
		return service.newOrder(order);
	}

	@PutMapping(value = "/")
	@CrossOrigin
	public Order editOrder(@RequestBody Order order) {
		return service.saveOrder(order);
	}

	@DeleteMapping(value = "/{id}")
	@CrossOrigin
	public int deleteOrder(@PathVariable int id) {
		return service.deleteOrder(id);
	}

	@GetMapping(value = "/filter")
	@CrossOrigin
	public List<Order> getOrdersByTicker(@RequestParam(defaultValue = "", required = true) String ticker) {

		return service.getOrdersByTicker(ticker);
	}

	@GetMapping(value = "/totalVolume")
	@CrossOrigin
	public HashMap<String, Integer> getTotalVolume(@RequestParam(defaultValue = "", required = true) String ticker) {

		return service.getTotalVolume(ticker);
	}

	@GetMapping(value = "/amountInvested")
	@CrossOrigin
	public HashMap<String, Float> getAmountInvested(@RequestParam(defaultValue = "", required = true) String ticker) {

		return service.getAmountInvested(ticker);
	}

	@GetMapping(value = "/volumeList")
	@CrossOrigin
	public List<HashMap<String, String>> getVolumeList() {

		return service.getVolumeDetails();
	}

	@GetMapping(value = "/amountList")
	@CrossOrigin
	public List<HashMap<String, String>> getAmountList() {

		return service.getAllAmountDetails();
	}

}
